<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('message');
            $table->integer('user_id')->index()->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->ondelete('set null');
            $table->integer('creator_id')->index()->unsigned()->nullable();
            $table->foreign('creator_id')->references('id')->on('creators')->ondelete('set null');                
            $table->integer('post_id')->index()->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')->ondelete('restrict');                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
