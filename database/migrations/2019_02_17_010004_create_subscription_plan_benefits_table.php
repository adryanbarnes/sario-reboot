<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPlanBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_plan_benefits', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('benefit');
            $table->integer('subscription_plan_id')->index()->unsigned();
            $table->foreign('subscription_plan_id')->references('id')->on('subscription_plans')->onupdate('cascade')->ondelete('restrict');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_plan_benefits');
    }
}
