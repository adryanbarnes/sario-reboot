<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_wallet_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('balance_before', 8, 2);
            $table->decimal('amount', 8, 2);
            $table->decimal('balance_after', 8, 2);
            $table->string('description');          
            $table->integer('user_wallet_id')->index()->unsigned();
            $table->foreign('user_wallet_id')->references('id')->on('user_wallets')->ondelete('restrict');              
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_wallet_transactions');
    }
}
