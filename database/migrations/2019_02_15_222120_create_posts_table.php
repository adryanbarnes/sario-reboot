<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('content');
            $table->integer('creator_id')->index()->unsigned();
            $table->foreign('creator_id')->references('id')->on('creators')->ondelete('restrict'); 
            $table->integer('post_status_id')->index()->unsigned();
            $table->foreign('post_status_id')->references('id')->on('post_statuses')->onupdate('cascade')->ondelete('restrict');   
            $table->integer('channel_id')->index()->unsigned()->nullable();
            $table->foreign('channel_id')->references('id')->on('channels')->onupdate('cascade')->ondelete('restrict');
            $table->dateTime('publish_at')->nullable();                                     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
