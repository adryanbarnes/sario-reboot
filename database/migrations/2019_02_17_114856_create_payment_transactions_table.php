<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone_number', 16)->nullable();
            $table->decimal('amount', 8, 2);
            $table->string('invoice');
            $table->integer('payment_method_id')->index()->unsigned();
            $table->foreign('payment_method_id')->references('id')->on('users')->ondelete('restrict');   
            $table->integer('mobile_network_id')->index()->unsigned()->nullable();
            $table->foreign('mobile_network_id')->references('id')->on('mobile_networks')->ondelete('restrict');              
            $table->unsignedInteger('payment_status_id');
            $table->foreign('payment_status_id')->references('id')->on('payment_statuses');
            $table->string('status_message');
            $table->string('payment_processor_reference')->nullable();
            $table->dateTime('processed_at')->nullable();                      
            $table->integer('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->ondelete('restrict'); 
            $table->integer('currency_id')->index()->unsigned();
            $table->foreign('currency_id')->references('id')->on('currencies')->onupdate('cascade')->ondelete('restrict');                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transactions');
    }
}
