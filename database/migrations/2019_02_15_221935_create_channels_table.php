<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description');
            $table->string('cover_image');
            $table->string('genre');
            $table->integer('category_id')->index()->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onupdate('cascade')->ondelete('set null');               
            $table->integer('creator_id')->index()->unsigned();
            $table->foreign('creator_id')->references('id')->on('creators')->ondelete('restrict'); 
            $table->integer('featured_channel_id')->index()->unsigned()->nullable();
            $table->foreign('featured_channel_id')->references('id')->on('channels')->ondelete('restrict');                         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels');
    }
}
