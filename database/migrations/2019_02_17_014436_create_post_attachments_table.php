<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attachment');
            $table->string('attachment_mime_type', 100);
            $table->integer('post_id')->index()->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')->onupdate('cascade')->ondelete('restrict');             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_parts');
    }
}
