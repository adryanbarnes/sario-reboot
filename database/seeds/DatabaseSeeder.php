<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(InterestsTableSeeder::class);
        $this->call(PostStatusesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
        $this->call(NetworksTableSeeder::class);    
        $this->call(PaymentStatusesTableSeeder::class);
        $this->call(CreatorsTableSeeder::class);  
        $this->call(PostTypesTableSeeder::class);   
        $this->call(AddTextToPostTypesTableSeeder::class);  
        
    }
}
