<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interests')->insert( array(
              ['name'=>'Hiplife', 'category_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
              ['name'=>'Rap', 'category_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
              ['name'=>'Afro Soul', 'category_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
              ['name'=>'Jazz', 'category_id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
              ['name'=>'HipHop', 'category_id' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
         ));   
    }
}
