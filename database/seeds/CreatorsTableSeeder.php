<?php

use Illuminate\Database\Seeder;

class CreatorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('creators')->insert([
            [
                'name' => "Adrian Koomson-Barnes",
                'email' => 'adrianbarnes@hotmail.co.uk',
                'phone_number' => '+233552511192',
                'password' => bcrypt('secretAdrian'),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now()
            ],
            [
                'name' => "Etornam",
                'email' => 'kudoloetornam@gmail.com',
                'phone_number' => '+233547158879',
                'password' => bcrypt('secretEtor'),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now()
            ]            
        ]);
    }
}
