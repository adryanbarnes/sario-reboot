<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/upload-media', 'APIAuthController@uploadFile');
Route::get('/currency', 'UtilitiesController@getCurrency');
Route::get('/interests', 'UtilitiesController@getInterests');
Route::get('/creators', 'UtilitiesController@getCreators');
Route::get('/get-post-comments/{post_id}/user/{user_id}/user-type/{type}', 'UtilitiesController@getPostComments');
Route::get('/get-post-likes/{post_id}/user/{user_id}/user-type/{type}', 'UtilitiesController@getPostLikes');
Route::get('/get-post-status', 'UtilitiesController@getPostStatus');
Route::get('/get-post-types', 'UtilitiesController@getPostTypes');
Route::get('interests', 'UtilitiesController@getInterests');
Route::get('payment-providers', 'UtilitiesController@paymentProviders');
Route::get('payment-status', 'UtilitiesController@paymentStatus');

//Consumer
Route::post('/consumer/add-user', 'APIAuthController@createUser');
Route::post('/consumer/{user_id}/update', 'APIAuthController@updateUser');
Route::post('/consumer/user-sign-in', 'APIAuthController@userSignIn');

Route::post('/consumer/subscribe', 'ConsumerController@subscribe');
Route::get('consumer/interests/{consumer_id}', 'ConsumerController@getConsumerInterests');
Route::post('consumer/interests', 'ConsumerController@postConsumerInterests');
Route::get('consumer/subscriptions/{consumer_id}', 'ConsumerController@getConsumerSubscriptions');
Route::post('consumer/channel/follow', 'ConsumerController@consumerFollowChannel');
Route::post('consumer/channel/unfollow', 'ConsumerController@consumerUnfollowChannel');
Route::get('consumer/feeds/{consumer_id}', 'ConsumerController@consumerChannelPosts');

Route::get('/consumer/wallet-balance', 'ConsumerController@getWalletBalance');
Route::post('/consumer/add-funds', 'ConsumerController@addFunds');
Route::post('/consumer/search-creator', 'ConsumerController@searchCreator');

Route::post('/consumer/comment', 'UtilitiesController@postComment');
Route::post('/consumer/uncomment', 'UtilitiesController@unComment');
Route::post('/consumer/like', 'UtilitiesController@postLike');
Route::post('/consumer/unlike', 'UtilitiesController@unLike');
Route::get('consumer/creators', 'UtilitiesController@getCreators');
Route::get('/consumer/recommendations/{consumer_id}', 'UtilitiesController@getConsumerRecommendations');



//Creator
Route::post('/creator/add-user', 'APIAuthController@createCreator');
Route::post('/creator/{creator_id}/update', 'APIAuthController@updateCreator');
Route::post('/creator/user-sign-in', 'APIAuthController@CreatorSignIn');
Route::get('/creator/categories', 'CreatorController@getCategory');
Route::post('/creator/setup-channel', 'CreatorController@setupChannel');
Route::get('/creator/channels/{creator_id}', 'CreatorController@getChannels');
Route::post('/creator/channels/{channel_id}/update', 'CreatorController@updateChannel');
Route::post('/creator/channels/{channel_id}/subscription-plans/{subscription_plan}/update', 'CreatorController@updateSubscriptionPlan');
Route::get('/creator/channel-posts/{creator_id}/{channel_id}', 'CreatorController@getChannelPost');
Route::post('/creator/post', 'CreatorController@createPost');
Route::get('/creator/delete-post/{id}', 'CreatorController@destroyPost');

Route::post('/creator/comment', 'UtilitiesController@postComment');
Route::post('/creator/uncomment', 'UtilitiesController@unComment');
Route::post('/creator/like', 'UtilitiesController@postLike');
Route::post('/creator/unlike', 'UtilitiesController@unLike');
Route::get('/creator/profile/stats/{creator_id}', 'CreatorController@creatorProfileStats');
