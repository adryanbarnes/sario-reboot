<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{	
    use SoftDeletes;
    
	protected $guarded = [];

    public function creator(){
    	return $this->belongsTo('App\Creator');
    }

    public function channel(){
    	return $this->belongsTo('App\Channel');
    }    

    public function postStatus(){
    	return $this->belongsTo('App\PostStatus');
    }    

    public function postAttachment(){
        return $this->hasMany('App\PostAttachment');
    }

    public function like(){
        return $this->hasMany('App\Like');
    }        

    public function comment(){
        return $this->hasMany('App\Comment');
    }      

}
