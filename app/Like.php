<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $guarded = [];

    public function user(){
		return $this->belongsTo('App\User');    	
    }

    public function creator(){
		return $this->belongsTo('App\Creator');    	
    }    

    public function post(){
		return $this->belongsTo('App\Post');    	
    }    

}
