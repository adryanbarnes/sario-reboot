<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    protected $guarded = [];

    public function paymentStatus(){
    	return $this->belongsTo('App\PaymentStatus');
    }    

    public function user(){
    	return $this->belongsTo('App\User');
    }       

}
