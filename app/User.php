<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userInterests(){
        return $this->hasMany('App\UserInterest');
    }

    public function userWallet(){
        return $this->hasOne('App\UserWallet');
    }    

    public function comment(){
        return $this->hasMany('App\Comment');
    }

    public function channels(){
        return $this->hasMany('App\UserChannelFollow');
    }

    public function like(){
        return $this->hasMany('App\Like');
    }    

}
