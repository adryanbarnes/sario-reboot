<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{	
	protected $guarded = [];
	
     public function user(){
    	return $this->belongsTo('App\User');
    }

    public function channel(){
    	return $this->belongsTo('App\Channel');
    }

    public function subscription_plan(){
    	return $this->belongsTo('App\SubscriptionPlan');
    }
}
