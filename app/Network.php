<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
    public function paymentMethod(){
    	return $this->belongsTo('App\PaymentMethod');
    } 
}
