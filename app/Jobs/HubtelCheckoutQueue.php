<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use App\Hubtel;
use App\PaymentTransaction;
use OVAC\LaravelHubtelPayment\Facades\HubtelPayment;

use App\Http\Controllers\PaymentProcessorController;

use \GuzzleHttp\Client as Guzzle;
use Carbon\Carbon;
use Log;

class HubtelCheckoutQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $payment;

    public function __construct(PaymentTransaction $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->payment->payment_status_id == 1){

            $this->initiate($this->payment);

            $this->payment->payment_status_id = 2;
            $this->payment->save();

        } 
    }

    public function initiate($payment){
        // $client = new Guzzle;
        // $payload = array();
        // $url  = Hubtel::CHARGE_URL;

        // $payload['items'] = array(
        //   'name'           => 'Sario Subscription',
        //   'quantity'       => 1,
        //   'unitPrice'      => $payment->amount
        // );
  
        // $payload['totalAmount']   = $payment->amount;
        // $payload['description']   = 'Sario Subscription';
  
        // $payload['callbackUrl'] =  route('payment-processor.hubtel.webhook');
        // $payload['returnUrl'] = "http://sario.xyz/";
        // $payload['merchantBusinessLogoUrl'] = Hubtel::ACCOUNT_NUMBER;
        // $payload['cancellationUrl'] = "http://sario.xyz/";
        // $payload['clientReference'] = $payment->invoice; 
      
        // $headers = [
        //   'Content-Type'    =>'application/json',
        //   'Authorization'   => 'Bearer '. base64_encode( Hubtel::CLIENT_ID.':'.Hubtel::CLIENT_SECRET)
        //   ];               
  
        // $params = ['headers'=>$headers,'json' => $payload,"verify"=>false];  

        try {
            $channel = null;
            switch ($payment->network_id) {
                case 1:
                  $channel = 'mtn-gh';
                  break;
                
                  case 2:
                  $channel = 'tigo-gh';
                  break;

                  case 3:
                  $channel = 'vodafone-gh';
                  break;
                
                  case 4:
                  $channel = 'airtel-gh';
                  break;                  
            }

            Log::debug('Posting To Hubtel');
            Log::debug($payment->user->name);
            //Log::debug($params);

            $post = HubtelPayment::ReceiveMoney()
            ->from($payment->phone_number)                //- The phone number to send the prompt to. 
            ->amount($payment->amount)                    //- The exact amount value of the transaction
            ->description('Sario Subscription')    //- Description of the transaction.
            ->customerName($payment->user->name)     //- Name of the person making the payment.callback after payment. 
            ->reference($payment->invoice)
            ->channel($channel)                 //- The mobile network Channel.configuration
            ->run(); //- Run the transaction after required data.  


            
            Log::debug((array) $post);

            $post = (array) $post;
            
            $payment->payment_processor_reference = $post['Data']->TransactionId;
            $payment->status_message =  $post['Data']->Description ;
            $payment->save();            

        //     $response = $client->request('POST', $url, $params);

        //    if($response->getStatusCode() == '200'){

        //        $stringResponse = $response->getBody()->getContents();
        //        $data = json_decode($stringResponse,true);


        //        Log::debug('Hubtel Response');
        //        Log::debug($data);

        //        if('Success' == $data['status']){

        //            $payment->payment_processor_reference = $data['checkoutId'];
        //            $payment->status_message =  !empty($data['message']) ? $data['message'] : "Successful Acknowledgement" ;
        //            $payment->processed_at = Carbon::now();
        //            $payment->save();

        //        }else{

        //             if($this->attempts() < 3){
                    
        //                 $this->release(45);

        //             }else{
        //                 $payment->payment_processor_reference = !empty($data['checkoutId']) ? $data['checkoutId'] : null;
        //                 $payment->status_message = !empty($data['message']) ? $data['message'] : "Payment Failed";
        //                 $payment->payment_status_id = 3;
        //                 $payment->processed_at = Carbon::now();
        //                 $payment->save();

        //                 app(PaymentProcessorController::class)->postNotification($payment);

        //             }
                  
        //         } 
        //    }
        // } catch (\GuzzleHttp\Exception\ClientException $e) {

        //     if ($e->hasResponse()) {
        //         $stringResponse = $e->getResponse()->getBody()->getContents();
        //         $data = json_decode($stringResponse,true);
        //         Log::debug($data);

        //         $payment->status_message = !empty($data['message']) ? $data['message'] : "Payment Failed Due to Unknown Error";
        //         $payment->payment_status_id = 3;
        //         $payment->processed_at = Carbon::now();
        //         $payment->save();

        //         app(PaymentProcessorController::class)->postNotification($payment);

        //     }else{

        //         $payment->status_message = "Request Could not be Verified due to an unknown error";
        //         $payment->payment_status_id = 3;
        //         $payment->processed_at = Carbon::now();
        //         $payment->save();

        //         app(PaymentProcessorController::class)->postNotification($payment);
        //     }


          
        }catch(Exception $ex1){

                $payment->status_message = "Request Could not be Failed due to an unknown error";
                $payment->payment_status_id = 3;
                $payment->processed_at = Carbon::now();
                $payment->save();

                app(PaymentProcessorController::class)->postNotification($payment);

                Log::debug('Request Could not be made due to : '.$ex1->getMessage());
        }        

    }
}
