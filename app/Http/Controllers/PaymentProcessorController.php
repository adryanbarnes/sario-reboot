<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\PaymentTransaction;
use App\UserWalletTransaction;
use App\UserWallet;

use App\Hubtel;

use App\Jobs\HubtelCheckoutQueue;

use Log;
use DB;
use Carbon\Carbon;
use Edujugon\PushNotification\PushNotification;

class PaymentProcessorController extends Controller
{
    public function initPayment($payment){

      Log::debug($payment);

      switch ($payment->payment_method_id) {
        case 1:
          $job = (new HubtelCheckoutQueue($payment));
          $this->dispatch($job);
          Log::debug("Hubtel Payment issued");
          break;
        
        default:
          # code...
          break;
      }      

    } 
    
    public function hubtelWebhook(Request $request){
      Log::debug("Hubtel Response");
      Log::debug($request->all());

      if($request->input('ResponseCode') == '0000'){
        $invoice = $request->input('Data.ClientReference');
        $transactionId = $request->input('Data.TransactionId');
        $externalTransactionId = $request->input('Data.ExternalTransactionId');
        $statusMessage = $request->input('Data.Description');

        Log::debug("Invoice Success");
        Log::debug($transactionId);

        $payment = PaymentTransaction::where('invoice', $invoice )->where('payment_processor_reference', $transactionId )->first();

        if($payment){

          $payment->update([
            'payment_status_id' => 4,
            'processed_at' => Carbon::now(),
            'extra_payment_detail' => $externalTransactionId,
            'status_message' => $statusMessage
          ]);
        }

        $this->topUpBalance($payment);
            
      }else if($request->input('ResponseCode')){
        $invoice = $request->input('Data.ClientReference');
        $transactionId = $request->input('Data.TransactionId');
        $externalTransactionId = $request->input('Data.ExternalTransactionId');
        $statusMessage = $request->input('Data.Description');

        Log::debug("Invoice Failed");
        Log::debug($transactionId);

        $payment = PaymentTransaction::where('invoice', $invoice )->where('payment_processor_reference', $transactionId )->first();

        if($payment){

          $payment->update([
            'payment_status_id' => 3,
            'processed_at' => Carbon::now(),
            'extra_payment_detail' => $externalTransactionId,
            'status_message' => $statusMessage
          ]);
        }
      }

      return response()->json(['success' => 'success'], 200);
    }

    public function topUpBalance(PaymentTransaction $payment){
      
      DB::beginTransaction();   

      $userWallet = UserWallet::where('user_id', $payment->user_id)->first();

      UserWalletTransaction::create([
        'balance_before'            => $userWallet->balance ,
        'amount'                    => $payment->amount,
        'balance_after'             => $userWallet->balance + $payment->amount,
        'description'               => "Wallet Top Up",
        'user_wallet_id'            => $userWallet->id,
        'payment_transaction_id'    => $payment->id
      ]);

      $userWallet->update(['balance' => $userWallet->balance + $payment->amount]);

      DB::commit();      

      $this->postNotification($payment);

      return;

    }

    public function postNotification($payment){

        $device = Device::where('user_id','=', $payment->user_id)->pluck('device_code')->toArray();

        Log::debug("Device code");
        Log::debug($device);

        $status =  $payment->paymentStatus->id == 4 ? 'successful' : "not successful";

        $transaction = array("invoice" => $payment->invoice, 'status' => $payment->paymentStatus->name, 'amount' => $payment->amount);
        Log::debug($transaction);

        $push = new PushNotification('fcm');

        $push->setMessage([
               'notification' => [
                       'title'=>'Wallet Top Up',
                       'body'=>  'Your wallet topup was ' . $status,
                       'sound' => 'default'
                       ],
               'data' => [
                       'transaction' => $transaction
                       ]
               ])
            ->setDevicesToken($device)
            ->send()
            ->getFeedback();

        Log::debug("Feedback");
        Log::debug((array) $push);

        return;

    }
}
