<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Interest;
use App\UserInterest;
use App\Subscription;
use App\UserWallet;
use App\PaymentTransaction;
use App\SubscriptionPlan;
use App\UserWalletTransaction;
use App\Creator;
use App\UserSearchTerm;
use App\UserChannelFollow;
use App\User;
use App\Post;
use App\Like;
use App\Network;

use Propaganistas\LaravelPhone\PhoneNumber;

use Log;
use DB;
use Validator;
use Carbon\Carbon;

class ConsumerController extends Controller
{
	public function postConsumerInterests(Request $request)
	{
		$input = $request->all();
		$user = User::find($input['user_id']);
		$interests = [];
		foreach ($input['interests'] as $interest) {
			$interests[] = [
				'interest_id' => $interest['id']
			];
		}
		$user->userInterests()->createMany($interests);
		return response()->json(['response_code' => '1000', 'response_message' => 'User Interest Created Successfully']);
	}
    public function getConsumerInterests($creatorId){

    	$consumerInterests = UserInterest::with('interest')->whereUserId($creatorId)->get();

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('consumer_interests' => $consumerInterests)]);
    }

    public function getConsumerSubscriptions($creatorId)
    {
    	$consumerSubscriptions = Subscription::with(['channel','subscription_plan'])->whereUserId($creatorId)->get();

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('consumer_subscriptions' => $consumerSubscriptions)]);
    }

    public function subscribe(Request $request){

		$validator = Validator::make($request->all(),[
		    'user_id'                                     => 'bail|required|exists:users,id',
		    'subscription_plan_id'                        => 'bail|required|exists:subscription_plans,id',
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
	    } 

	    DB::beginTransaction();   

	    $subscription = SubscriptionPlan::find($request->get('subscription_plan_id'));

	    $userWallet = UserWallet::where('user_id', $request->get('user_id'))->first();

	    if($userWallet->balance < $subscription->amount){

	    	return response()->json(['response_code' => "1001", 'message' => 'Insufficient Balance']);
	    }

	    Subscription::create([
	    	'subscription_plan_id'    => $subscription->id,
	    	'user_id'                 => $request->get('user_id'),
	    	'channel_id'              => $subscription->channel_id,
	    	'subscription_end'        => Carbon::now()->addDays($subscription->duration)
	    ]);

	    UserWalletTransaction::create([
	    	'balance_before'            => $userWallet->balance ,
	    	'amount'                    => $subscription->amount,
	    	'balance_after'             => $userWallet->balance - $subscription->amount,
	    	'description'               => "Subscriptions",
	    	'user_wallet_id'            => $userWallet->id,
	    	'channel_id'                => $subscription->channel_id
	    ]);

	    $userWallet->update(['balance' => $userWallet->balance - $subscription->amount]);

	    DB::commit();

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('subscription' => $subscription)]);
    }

    public function getWalletBalance($userId){
    	$balance = UserWallet::whereUserId($userId)->first();

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('wallet_balance' => $balance)]);
    }

    public function addFunds(Request $request){

		$validator = Validator::make($request->all(),[
		    'phone_number'                                => 'bail|required|phone:GH',
		    'user_id'                                     => 'bail|required|exists:users,id',
		    'payment_method_id'                           => 'bail|required|exists:payment_methods,id',
		    'amount'                                      => 'bail|required|numeric',	    
		    'network_id'                                  => 'bail|required_if:payment_method_id,1|exists:networks,id',
		  	'currency_id'                                 => 'bail|required|exists:currencies,id'
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
		} 
		
		$network = Network::wherePaymentMethodId($request->get('payment_method_id'))->find($request->get('network_id'));

	    if (!$network) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => "payment method doesn't correspond with network"
	        ]);
	    } 

	    $paymentReceipt = PaymentTransaction::create([
	    	'user_id'                      => $request->get('user_id'),
	    	'phone_number'                 => PhoneNumber::make($request->get('phone_number'), 'GH')->formatE164(),
	    	'network_id'                   => $request->get('network_id'),
	    	'amount'                       => $request->get('amount'),
	    	'payment_method_id'            => $request->get('payment_method_id'),
	    	'payment_status_id'            => 1,
	    	'invoice'                      => str_random(10),
	    	'currency_id'                  => $request->get('currency_id'),
	    	'status_message'               => "Payment Request Issued"

		]);

		app(PaymentProcessorController::class)->initPayment($paymentReceipt);

		$paymentReceipt->load('paymentStatus');

		

	    return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('fund_request' => $paymentReceipt) ]);

    }

    public function consumerFollowChannel(Request $request)
    {
    	$user_follow = UserChannelFollow::create($request->all());
    	return response()->json(['response_code' => '1000', 'response_message' => 'Success']);
    }

    public function consumerUnfollowChannel(Request $request)
    {
    	$input = $request->all();
    	$user_unfollow = UserChannelFollow::whereUserId($input['user_id'])->whereChannelId($input['channel_id'])->first();
    	$user_unfollow->delete();
    	return response()->json(['response_code' => '1000', 'response_message' => 'Channel unfollowed successfully']);
    }

    public function consumerChannelPosts($userId)
    {
    	$active_subscriptions = [];
    	$user_subscriptions = Subscription::whereUserId($userId)->get();
    	foreach ($user_subscriptions as $subscription) {
    		$subscription_end = Carbon::parse($subscription->subscription_end);
    		if ($subscription_end >= Carbon::now()) {
    			$active_subscriptions[] = $subscription->channel_id;
    		}
    	}
		$posts = Post::with(['creator','postAttachment', 'channel'])->withCount('like', 'comment')
			->whereIn('channel_id',$active_subscriptions)->get();


		$posts = $posts->map(function ($item, $key) use ($userId){
			$item->liked = Like::whereUserId($userId)->wherePostId($item->id)->first() ? true : false;
			return $item;
		});

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array(
    		'posts' => $posts) ]);
    }

    public function searchCreator(Request $request){

		$validator = Validator::make($request->all(),[
		    'user_id'  => 'bail|required|exists:users,id',
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
	    } 

        $keyword = $request->get('search');

        $interestFilter = $request->get('interest_filter');

        $creators = Creator::where(function ($query) use ($keyword) {
            // $query->where("name","like", "%$keyword%")->orWhere("surname", "like", "%$keyword%");
            $query->where("name","like", "%$keyword%");
        })
        ->get();

        $creators->load(['channels']);

		if($creators->isNotEmpty() && $request->get('search')){

			UserSearchTerm::firstOrCreate(
				['term' => $request->get('search')],
				['user_id' => $request->get('user_id')]
			);

		}

		$history = UserSearchTerm::where('user_id', $request->get('user_id'))->pluck('term');

        return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('search_result' => $creators, 'search_histoy' => $history) ]);
    }
}
