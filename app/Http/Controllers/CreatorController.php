<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\Interest;
use App\Creator;
use App\Channel;
use App\SubscriptionPlan;
use App\Currency;
use App\SubscriptionPlanBenefit;
use App\PostStatus;
use App\Post;
use App\PostAttachment;
use App\UserChannelFollow;
use App\Subscription;
use App\Like;
use App\Comment;
use App\PostType;

use DB;
use Validator;
use Log;
use Carbon\Carbon;
use App\Exceptions\Handler;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CreatorController extends Controller
{
    public function getCategory(){
    	$categories = Category::with('genre')->get();

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('categories' => $categories)]);    	
    }

    public function setupChannel(Request $request){

		Log::debug($request->all());

		$validator = Validator::make($request->all(),[
		    'channel_info.name'                        => 'bail|required|max:255',
		    'channel_info.cover_image'                 => 'bail|required|url',
		    'channel_info.description'                 => 'bail|required|string',	    
		    'channel_info.category_id'                 => 'bail|required|exists:categories,id',
		    'channel_info.genre'                       => 'bail|required',
		    'about_you.creator_id'                     => 'bail|required|exists:creators,id',
		    'about_you.profile_picture'                => 'bail|required|url',
		    'about_you.description'                    => 'bail|required|string',

		    'benefit.*.title'                          => 'bail|required|string',
		    'benefit.*.list'                           => 'bail|required|array',
		    'benefit.*.currency_id'                    => 'bail|required|exists:currencies,id',
		    'benefit.*.price'                          => 'bail|required|numeric',
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
	    }   

	    try{

		    $creator = Creator::find($request->input('about_you.creator_id'));

		    $path = 'uploads/'.$creator->name ;

			$channelPath = $path.'/channel/cover-image/';
			
			$localChannelFile = Str::after($request->input('channel_info.cover_image'), 'temp/');

			Log::debug($localChannelFile);

	       // Storage::disk('public')->putFileAs($channelPath, new File($request->file('channel_info.cover_image')), $channelCoverImageName); 

			if($localChannelFile != $request->input('channel_info.cover_image')){
				$channelCoverImageName = "Channel_". kebab_case($request->input('channel_info.name')). "_". str_random(5).'.' .Str::after($localChannelFile, '.');
				Storage::disk('public')->move('temp/' .$localChannelFile, $channelPath. $channelCoverImageName);

				$channelCoverImageName = $channelPath. $channelCoverImageName;

			}else{
				$channelCoverImageName = $request->input('channel_info.cover_image');
			}	
			
			
	        $profilePicturePath = $path.'/profile-pictures/';

	        //Storage::disk('public')->putFileAs($profilePicturePath, new File($request->file('about_you.profile_picture')), $profilePictureName);         

			$localProfileFile = Str::after($request->input('about_you.profile_picture'), 'temp/');
			if($localProfileFile  != $request->input('about_you.profile_picture')){
				$profilePictureName = "profile_picture_". str_random(20).'.'. Str::after($localProfileFile, '.');

				Storage::disk('public')->move('temp/' .$localProfileFile, $profilePicturePath. $profilePictureName);

				$profilePictureName = $channelPath. $profilePictureName;

			}else{
				$profilePictureName = $request->input('about_you.profile_picture');
			}			

			if ($channelCoverImageName && $profilePictureName) {
				DB::beginTransaction(); 

				$channel = Channel::create([
					'name'              => $request->input('channel_info.name'),
					'description'       => $request->input('channel_info.description'),
					'category_id'       => $request->input('channel_info.category_id'),
					'cover_image'       => $channelCoverImageName,
					'genre'             => $request->input('channel_info.genre'),
					'creator_id'        => $request->input('about_you.creator_id')
				]);

				$creator->update([
					'profile_picture'    => $profilePictureName,
					'description'        => $request->input('about_you.description')
				]);


				foreach ($request->input('benefit') as $benefit) {
					Log::debug($benefit);

					$subscriptionPlan = SubscriptionPlan::create([
						'name'           => $benefit['title'],
						'amount'         => $benefit['price'],
						'duration'       => 30,
						'channel_id'     => $channel->id,
						'currency_id'    => $benefit['currency_id']
					]);

					foreach ($benefit['list'] as $list) {
						SubscriptionPlanBenefit::create([
							'benefit'                  => $list,
							'subscription_plan_id'     => $subscriptionPlan->id 
						]);
					}


				}


				DB::commit();

				$channel->cover_image = $localChannelFile ? asset('storage/'.$channel->cover_image) : $channel->cover_image;

				$creator->profile_picture = $localProfileFile ? asset('storage/'.$creator->profile_picture) : $creator->profile_picture;

			    return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('channel' => $channel, 'creator' => $creator)]);  

			}

			return response()->json(['response_code' => "1001", 'message' => 'Error occured when setting up channel']);

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json(['response_code' => "1001", 'message' => 'Error occured when trying to setting up channel']);             
        }        
    	
    }

    public function createPost(Request $request){
    	Log::debug($request->all());

		$validator = Validator::make($request->all(),[
		    'channel_id'                                => 'bail|required|exists:channels,id',
		    'content'                                   => 'bail|nullable|string',
		    'post_status_id'                            => 'bail|required|exists:post_statuses,id',	    
		    'creator_id'                                => 'bail|required|exists:creators,id',
		    'publish_at'                                => 'bail|nullable|date',
			'post_type_id'                              => 'bail|required|exists:post_types,id',
			'post_attachment'                           => 'bail|required_unless:post_type_id,5|array',
			'post_attachment.*'                         => 'bail|required_unless:post_type_id,5|url',
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
	    } 

	    try{

	    	DB::beginTransaction(); 

		    $post = Post::create([
		    	'content' => $request->input('content'),
		    	'creator_id' => $request->input('creator_id'),
				'post_status_id' => $request->input('post_status_id'),
				'post_type_id' => $request->input('post_type_id'),
		    	'channel_id' => $request->input('channel_id'),
		    	'publish_at' => $request->input('publish_at')
		    ]);

		    $creator = Creator::find($request->input('creator_id'));

		    $channel = Channel::find($request->input('channel_id'));

		    $path = 'uploads/'.$creator->name .'/channel/post_'.$channel->id .'/' ;

			//$attachments = $request->hasFile('post_attachment') ? $request->file('post_attachment') : [];

			$attachments = $request->input('post_attachment') ? $request->input('post_attachment') : [];

		    foreach ($attachments as $attachment) {

		    	Log::debug($attachments);

				$localFile = Str::after($attachment, 'temp/');
				
				if($localFile != $attachment){
					//$attachmentLink = "Post_Attachment_".$post->id. "_". str_random(5).'.'. $attachment->getClientOriginalExtension();

					//Storage::disk('public')->putFileAs($path, new File($attachment), $attachmentLink); 

					Storage::disk('public')->move('temp/' .$localFile, $path. $localFile);
	
					$fileName = $path. $localFile;
	
				}else{

					$fileName = $attachment;

				}
				
				$mimeType = Str::before(Str::after( $fileName, 'mimetype_'), '-');

				$mimeType = Str::replaceArray( '.',['/'],$mimeType);

		    	PostAttachment::create([
		    		'attachment' => $fileName,
		    		'attachment_mime_type' => $mimeType,
		    		'post_id'      => $post->id
		    	]);
		    } 

		    $post->load('postAttachment');

		    DB::commit();

	    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('post' => $post)]); 

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json(['response_code' => "1001", 'message' => 'Error occured when trying to creating post']);             
        }  


    }  

    public function getChannels($creatorId){
    	$channels = Channel::whereCreatorId($creatorId);

    	$channels = $channels->with(['posts' => function ($query) {
				$query->with(['postAttachment'])->withCount('like', 'comment'); 
			},
		    'benefit' => function ($query) { 
				$query->with('planBenefit'); 
			}						
		])->get();

		$channels = $channels->map(function ($item, $key) use ($creatorId){

			$item->posts = $item->posts->map(function ($childItem, $childKey) use ($creatorId){
				$childItem->liked = Like::whereCreatorId($creatorId)->wherePostId($childItem->id)->first() ? true : false;
				return $childItem;

			});

			return $item;
		});

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('channels' => $channels)]);     	
    }  

    public function getChannelPost($creatorId,$channelId ){

    	$channel = Channel::whereCreatorId($creatorId)->find($channelId);

    	$channel = $channel->posts()->with(['postAttachment'])->withCount(['like', 'comment'])->get();

    	$channel = $channel->map(function ($item, $key) use ($creatorId){
			$item->liked = Like::whereCreatorId($creatorId)->wherePostId($item->id)->first() ? true : false;
			return $item;
		});

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('channel' => $channel)]); 

    }

    public function creatorProfileStats($creatorId, $json = true)
    {
    	$stat_data = []; $subscribers = [];
    	$year = Carbon::now()->year;

    	$creator_details = Creator::find($creatorId);
    	$creator_channels = Channel::whereCreatorId($creatorId)->pluck('id');
    	$creator_posts_query = Post::whereCreatorId($creatorId);
    	$creator_posts = $creator_posts_query->pluck('id');
    	$stat_data['Total Followers'] = UserChannelFollow::whereIn('channel_id',$creator_channels)->count();
    	$stat_data['Total Subscriptions'] = Subscription::whereIn('channel_id',$creator_channels)->count();
    	$stat_data['Total Revenue'] = Subscription::join('subscription_plans','subscriptions.subscription_plan_id','=','subscription_plans.id')->whereIn('subscriptions.channel_id',$creator_channels)->sum('amount');
    	$stat_data['Total Likes'] = Like::whereIn('post_id',$creator_posts)->count();
    	$stat_data['Total Comments'] = Comment::whereIn('post_id',$creator_posts)->count();
    	$stat_data['Total Posts'] = $creator_posts_query->count();

    	foreach (range(1, 12) as $month) {
    		$subscribers[] = count(Subscription::whereIn('channel_id',$creator_channels)->whereRaw("MONTH(created_at) = $month and YEAR(created_at) = $year")->get());
		}

		$creator_details->load(['channels' => function ($query) {
			$query->with(['posts' => function ($query) {
				$query->with(['postAttachment', 'channel'])->withCount('like', 'comment'); 
			},
				'benefit' => function ($query) { 
				$query->with('planBenefit'); 
			}
			]);
		}]);

		$extraload = array('creator_details' => $creator_details, 'gen_stats' => $stat_data, 'subscribers_chart_data' => $subscribers);

		if(!$json)
			return $extraload;

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => $extraload]);

	}
	
	public function updateChannel($channelId, Request $request){
		Log::debug($request->all());		

		$validator = Validator::make($request->all(),[
		    'channel_info.name'                        => 'bail|required|max:255',
		    'channel_info.cover_image'                 => 'bail|required|url',
		    'channel_info.description'                 => 'bail|required|string',	    
		    'channel_info.category_id'                 => 'bail|required|exists:categories,id',
		    'channel_info.genre'                       => 'bail|required',
		    'creator_id'                     => 'bail|required|exists:creators,id',
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
		}  
		
		$channel = Channel::where('creator_id',$request->input('creator_id') )->find($channelId);

		if (!$channel) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => "Channel wasn't found"
	        ]);
		}  
		
	    try{

			$creator = Creator::find($request->input('creator_id'));

		    $path = 'uploads/'.$creator->name ;

			$channelPath = $path.'/channel/cover-image/';
			
			$localChannelFile = Str::after($request->input('channel_info.cover_image'), 'temp/');

			Log::debug($localChannelFile);

			if($localChannelFile != $request->input('channel_info.cover_image')){
				$channelCoverImageName = "Channel_". kebab_case($request->input('channel_info.name')). "_". str_random(5).'.' .Str::after($localChannelFile, '.');
				Storage::disk('public')->move('temp/' .$localChannelFile, $channelPath. $channelCoverImageName);

				$channelCoverImageName = $channelPath. $channelCoverImageName;

			}else{
				$channelCoverImageName = $request->input('channel_info.cover_image');
			}	
			DB::beginTransaction(); 

			$channel->update([
				'name'              => $request->input('channel_info.name'),
				'description'       => $request->input('channel_info.description'),
				'category_id'       => $request->input('channel_info.category_id'),
				'cover_image'       => $channelCoverImageName,
				'genre'             => $request->input('channel_info.genre')
			]);

			DB::commit();

			return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('channel' => $channel)]);  

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json(['response_code' => "1001", 'message' => 'Error occured when trying to updating channel']);             
        }  		
	}

	public function updateSubscriptionPlan($channelId, $subscriptionPlan, Request $request){
		$validator = Validator::make($request->all(),[
		    'creator_id'                     => 'bail|required|exists:creators,id',
		    'title'                          => 'bail|required|string',
		    'list.*'                         => 'bail|required|string',
		    'currency_id'                    => 'bail|required|exists:currencies,id',
		    'price'                          => 'bail|required|numeric',
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
		}   
		
		$channel = Channel::where('creator_id',$request->input('creator_id') )->find($channelId);
		if (!$channel) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => "Channel wasn't found"
	        ]);
		} 

		$subscriptionPlan = SubscriptionPlan::where('channel_id',$channelId )->find($subscriptionPlan);
		if (!$subscriptionPlan) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => "Subscription Plan wasn't found"
	        ]);
		} 
		try{
			
			$subscriptionPlan->update([
				'name'           => $request->input('title'),
				'amount'         => $request->input('price'),
				'duration'       => 30,				
				'currency_id'    => $request->input('currency_id')
			]);

			SubscriptionPlanBenefit::where('subscription_plan_id', $subscriptionPlan->id)->delete();

			foreach ($request->input('list') as $list) {
				SubscriptionPlanBenefit::create([
					'benefit'                  => $list,
					'subscription_plan_id'     => $subscriptionPlan->id 
				]);
			}

			DB::commit();

			$subscriptionPlan->load('planBenefit');

			return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('subscription_plan' => $subscriptionPlan)]); 
		
		}catch(\Exception $ex){

			report($ex);

			DB::rollBack();

			return response()->json(['response_code' => "1001", 'message' => 'Error occured when trying to setting up channel']);             
		}  			
	}

	public function destroyPost($id){

		$post = Post::find($id);

		if($post){
			$post->postAttachment()->delete();
			$post->delete();
		}

		return response()->json(['response_code' => '1000', 'response_message' => 'Successfully deleted']);
	}
	
}
