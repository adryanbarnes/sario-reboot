<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Device;
use App\Creator;
use App\UserWallet;

use DB;
use Carbon\Carbon;
use Log;
use Validator;
use Hash;
use App\Exceptions\Handler;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class APIAuthController extends Controller
{

	public function uploadFile(Request $request){

		$validator = Validator::make($request->all(),[
		    'file'             => 'required',
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
		} 	
		
		$bucket = 'sario-android.appspot.com';
		$path = 'media/';
		$fileName = 'mimetype_'. Str::replaceArray('/',['.'],$request->file('file')->getMimeType()) .'-'. str_random(10) . '.'. $request->file('file')->getClientOriginalExtension();
		//Storage::disk('public')->putFileAs($path, $request->file('file'), $fileName); 

		$disk = Storage::disk('gcs')->putFileAs($path, $request->file('file'), $fileName); 
		$fileName = 'https://storage.googleapis.com/'.$bucket.'/'. $path. $fileName;

		//Storage::disk('gcs')->put('avatars/1', $request->file('file'));
		return response()->json([
			'response_code' => "1000",
			'message' => 'File Succesfully Upload',
			// 'extra_payload' => asset('storage/temp/'.$fileName)
			'extra_payload' => $fileName
			]);
        
    }

    public function createUser(Request $request){

		$validator = Validator::make($request->all(),[
		    'name'                      => 'bail|required|max:255',
		    'device_id'                 => 'bail|required',
		    'device_code'               => 'bail|required',
		    // 'profile_picture'           => 'bail|required|url',		    
		    'email'                     => 'bail|nullable|email|unique:users',
		    'password'                  => 'bail|nullable',
		    'fb_user_id'                => 'bail|required_without_all:password,google_user_id|unique:users',
		    'google_user_id'            => 'bail|required_without_all:password,fb_user_id|unique:users'
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
	    } 

	    try {
	    	DB::beginTransaction(); 

		    $user = User::create([
		    	'name' => $request->get('name'),
		    	'email' => $request->get('email'),
		    	'password' => Hash::make($request->get('password')),
		    	'fb_user_id'  => $request->get('fb_user_id'),
		    	'google_user_id' => $request->get('google_user_id')

			]);		

		    UserWallet::create([
		    	'balance'         => 0,
		    	'user_id'         => $user->id,
		    	'currency_id'     => 1
		    ]);

		    Device::updateOrCreate(
		    	['user_id' => $user->id, 'device_id' => $request->get('device_id')],
		    	['device_code' => $request->get('device_code')]
		    );

			$localFile = Str::after($request->get('profile_picture'), 'temp/');

		    $path = 'uploads/'.$user->name .'/profile_image/';

			if($localFile != $request->get('profile_picture')){
				Storage::disk('public')->move('temp/' .$localFile, $path. $localFile);

				$fileName = $path. $localFile;

			}else{
				$fileName = $request->get('profile_picture');
			}	

			$user->update([
				'profile_picture' => $fileName
			]);

            DB::commit();

            if($request->get('email'))
            	$user->sendEmailVerificationNotification();

			$user->load('userWallet');
			
			$user->profile_picture = $localFile ? asset('storage/'.$user->profile_picture) : $user->profile_picture;

		    return response()->json(['response_code' => "1000", 'message' => 'Sign up successful', 'extra_payload' => array('user' => $user)]);

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json(['response_code' => "1001", 'message' => 'Error occured when trying to sign up']);             
        }   

	}  
	
    public function updateUser($userId,Request $request){

		$validator = Validator::make($request->all(),[
		    'name'                      => 'bail|required|max:255',
		    'device_id'                 => 'bail|required',
		    'device_code'               => 'bail|required',
		    'profile_picture'           => 'bail|required|url',		    
		    'email'                     => ['bail','nullable','email',Rule::unique('users')->ignore($userId)],
		    'password'                  => 'bail|nullable',
		    'fb_user_id'                => ['bail','required_without_all:password,google_user_id',Rule::unique('users')->ignore($userId)],
		    'google_user_id'            => ['bail','required_without_all:password,fb_user_id',Rule::unique('users')->ignore($userId)]
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
		} 
		
		$user = User::find($userId);

		if (!$user) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => "Unknown Consumer"
	        ]);
	    } 		

	    try {
	    	DB::beginTransaction(); 

		    $user->update([
		    	'name' => $request->get('name'),
		    	'email' => $request->get('email'),
		    	'password' => Hash::make($request->get('password')),
		    	'fb_user_id'  => $request->get('fb_user_id'),
		    	'google_user_id' => $request->get('google_user_id')

			]);		

		    Device::updateOrCreate(
		    	['user_id' => $user->id, 'device_id' => $request->get('device_id')],
		    	['device_code' => $request->get('device_code')]
		    );

			$localFile = Str::after($request->get('profile_picture'), 'temp/');

		    $path = 'uploads/'.$user->name .'/profile_image/';

			if($localFile != $request->get('profile_picture')){
				Storage::disk('public')->move('temp/' .$localFile, $path. $localFile);

				$fileName = $path. $localFile;

			}else{
				$fileName = $request->get('profile_picture');
			}	

			$user->update([
				'profile_picture' => $fileName
			]);

            DB::commit();

			$user->load('userWallet');
			
			$user->profile_picture = $localFile ? asset('storage/'.$user->profile_picture) : $user->profile_picture;

		    return response()->json(['response_code' => "1000", 'message' => 'Update successful', 'extra_payload' => array('user' => $user)]);

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json(['response_code' => "1001", 'message' => 'Error occured when trying to update']);             
        }   

    }   	

    public function userSignIn(Request $request){
		$validator = Validator::make($request->all(),[
		    //'name'                      => 'bail|required|max:255',
		    'device_id'                 => 'bail|required',
		    'device_code'               => 'bail|required',	    
		    'email'                     => 'bail|nullable|email',
		    'password'                  => 'bail|nullable',
		    'fb_user_id'                => 'bail|required_without_all:password,google_user_id',
		    'google_user_id'            => 'bail|required_without_all:password,fb_user_id'
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
	    }

	    //if user is logging in with email and password

		if ($request->get('email') && $request->get('password')){
			$user = User::whereEmail($request->get('email'))->first();

    		if($user && Hash::check($request->get('password'), $user->password)) {

			    Device::updateOrCreate(
			    	['user_id' => $user->id, 'device_id' => $request->get('device_id')],
			    	['device_code' => $request->get('device_code')]
			    );

    			$user->interest = $user->userInterests()->get();

    			return response()->json([
    				'response_code' => "1000",
    				'message' => 'Sign in successful',
    				'extra_payload' => $user
    			]);

    		}else{

    			return response()->json([
    				'response_code' => "1001",
    				'message' => 'Invalid credentials'
    			]);
    		}			
		}elseif ($request->get('fb_user_id')){
			$user = User::where('fb_user_id', $request->get('fb_user_id'))->first();

			if($user){

			    Device::updateOrCreate(
			    	['user_id' => $user->id, 'device_id' => $request->get('device_id')],
			    	['device_code' => $request->get('device_code')]
			    );

    			$user->interest = $user->userInterests()->get();

    			return response()->json([
    				'response_code' => "1000",
    				'message' => 'Sign in successful',
    				'extra_payload' => $user
    			]);	

    		}else{

    			return response()->json([
    				'response_code' => "1001",
    				'message' => 'Invalid credentials'
    			]);
    		}	

		}elseif ($request->get('google_user_id')){
			$user = User::where('google_user_id', $request->get('google_user_id'))->first();

			if($user){

			    Device::updateOrCreate(
			    	['user_id' => $user->id, 'device_id' => $request->get('device_id')],
			    	['device_code' => $request->get('device_code')]
			    );

    			$user->interest = $user->userInterests()->get();

    			return response()->json([
    				'response_code' => "1000",
    				'message' => 'Sign in successful',
    				'extra_payload' => $user
    			]);	

    		}else{

    			return response()->json([
    				'response_code' => "1001",
    				'message' => 'Invalid credentials'
    			]);
    		}				
		}else {
    			return response()->json([
    				'response_code' => "1001",
    				'message' => 'Invalid credentials'
    			]);
		}

    }

    public function createCreator(Request $request){
		$validator = Validator::make($request->all(),[
		    'name'                      => 'bail|required|max:255',
		    'device_id'                 => 'bail|required',
		    'device_code'               => 'bail|required',
		    //'profile_picture'           => 'bail|required|url',		    
		    'email'                     => 'bail|nullable|email|unique:creators',
		    'password'                  => 'bail|nullable',
		    'fb_user_id'                => 'bail|required_without_all:password,google_user_id|unique:creators',
		    'google_user_id'            => 'bail|required_without_all:password,fb_user_id|unique:creators'
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
	    } 

	    try {
	    	DB::beginTransaction(); 

		    $creator = Creator::create([
		    	'name' => $request->get('name'),
		    	'email' => $request->get('email'),
		    	'password' => Hash::make($request->get('password')),
		    	'fb_user_id'  => $request->get('fb_user_id'),
		    	'google_user_id' => $request->get('google_user_id')

		    ]);

		    Device::updateOrCreate(
		    	['creator_id' => $creator->id, 'device_id' => $request->get('device_id')],
		    	['device_code' => $request->get('device_code')]
			);
			
			$localFile = Str::after($request->get('profile_picture'), 'temp/');

		    $path = 'uploads/'.$creator->name .'/profile_image/';

			if($localFile != $request->get('profile_picture')){
				Storage::disk('public')->move('temp/' .$localFile, $path. $localFile);

				$fileName = $path. $localFile;

			}else{
				$fileName = $request->get('profile_picture');
			}	

			$creator->update([
				'profile_picture' => $fileName
			]);
		

			DB::commit();
			
			$creator->profile_picture = $localFile ? asset('storage/'.$creator->profile_picture) : $creator->profile_picture;

            if($request->get('email'))
            	$creator->sendEmailVerificationNotification();            

		    return response()->json(['response_code' => "1000", 'message' => 'Sign up successful', 'extra_payload' => array('creator' => $creator)]);

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json(['response_code' => "1001", 'message' => 'Error occured when trying to sign up']);             
        }
    }  

    public function updateCreator($creatorId, Request $request){
		$validator = Validator::make($request->all(),[
		    'name'                      => 'bail|required|max:255',
		    'device_id'                 => 'bail|required',
		    'device_code'               => 'bail|required',
		    //'profile_picture'           => 'bail|required|url',		    
		    'email'                     => ['bail','nullable','email',Rule::unique('creators')->ignore($creatorId)],
		    'password'                  => 'bail|required_with:email',
		    'fb_user_id'                => ['bail','required_without_all:email,google_user_id',Rule::unique('creators')->ignore($creatorId)],
		    'google_user_id'            => ['bail','required_without_all:email,fb_user_id',Rule::unique('creators')->ignore($creatorId)]
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
		} 
		
		$creator = Creator::find($creatorId);

		if (!$creator) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => "Unknown Creator"
	        ]);
	    } 

	    try {
	    	DB::beginTransaction(); 

		    $creator->update([
		    	'name' => $request->get('name'),
		    	'email' => $request->get('email'),
		    	'password' => Hash::make($request->get('password')),
		    	'fb_user_id'  => $request->get('fb_user_id'),
		    	'google_user_id' => $request->get('google_user_id')

		    ]);

		    Device::updateOrCreate(
		    	['creator_id' => $creator->id, 'device_id' => $request->get('device_id')],
		    	['device_code' => $request->get('device_code')]
			);
			
			$localFile = Str::after($request->get('profile_picture'), 'temp/');

		    $path = 'uploads/'.$creator->name .'/profile_image/';

			if($localFile != $request->get('profile_picture')){
				Storage::disk('public')->move('temp/' .$localFile, $path. $localFile);

				$fileName = $path. $localFile;

			}else{
				$fileName = $request->get('profile_picture');
			}	

			$creator->update([
				'profile_picture' => $fileName
			]);
		

			DB::commit();
			
			$creator->profile_picture = $localFile ? asset('storage/'.$creator->profile_picture) : $creator->profile_picture;           

		    return response()->json(['response_code' => "1000", 'message' => 'Update successful', 'extra_payload' => array('creator' => $creator)]);

        }catch(\Exception $ex){

            report($ex);

            DB::rollBack();

            return response()->json(['response_code' => "1001", 'message' => 'Error occured when trying to sign up']);             
        }
    }  	

    public function creatorSignIn(Request $request){
		$validator = Validator::make($request->all(),[
		    //'name'                      => 'bail|required|max:255',
		    'device_id'                 => 'bail|required',
		    'device_code'               => 'bail|required',	    
		    'email'                     => 'bail|nullable|email',
		    'password'                  => 'bail|nullable',
		    'fb_user_id'                => 'bail|required_without_all:password,google_user_id',
		    'google_user_id'            => 'bail|required_without_all:password,fb_user_id'
		]);    

	    if ($validator->fails()) {

	        return response()->json([
				'response_code' => '1001',
				'errors' => $validator->errors()
	        ]);
	    }

	    //if user is logging in with email and password

		if ($request->get('email') && $request->get('password')){
			$creator = Creator::whereEmail($request->get('email'))->first();

    		if($creator && Hash::check($request->get('password'), $creator->password)) {

			    Device::updateOrCreate(
			    	['creator_id' => $creator->id, 'device_id' => $request->get('device_id')],
			    	['device_code' => $request->get('device_code')]
				);

				$creator->load(['channels' => function ($query) {
					$query->with(['posts' => function ($query) {
						$query->with('postAttachment'); },
						'benefit' => function ($query) { 
						$query->with('planBenefit'); }
					]);
				}]);				
				
				$creator->profile_stats = app(CreatorController::class)->creatorProfileStats($creator->id, false);

    			return response()->json([
    				'response_code' => "1000",
    				'message' => 'Sign in successful',
    				'extra_payload' => $creator
    			]);

    		}else{

    			return response()->json([
    				'response_code' => "1001",
    				'message' => 'Invalid credentials'
    			]);
    		}			
		}elseif ($request->get('fb_user_id')){
			$creator = Creator::where('fb_user_id', $request->get('fb_user_id'))->first();

			if($creator){
			    Device::updateOrCreate(
			    	['creator_id' => $creator->id, 'device_id' => $request->get('device_id')],
			    	['device_code' => $request->get('device_code')]
			    );

				$creator->load(['channels' => function ($query) {
					$query->with(['posts' => function ($query) {
						$query->with('postAttachment'); },
						'benefit' => function ($query) { 
						$query->with('planBenefit'); }
					]);
				}]);	
				
				$creator->profile_stats = app(CreatorController::class)->creatorProfileStats($creator->id, false);

    			return response()->json([
    				'response_code' => "1000",
    				'message' => 'Sign in successful',
    				'extra_payload' => $creator
    			]);	

    		}else{

    			return response()->json([
    				'response_code' => "1001",
    				'message' => 'Invalid credentials'
    			]);
    		}	

		}elseif ($request->get('google_user_id')){
			$creator = Creator::where('google_user_id', $request->get('google_user_id'))->first();

			if($creator){

			    Device::updateOrCreate(
			    	['creator_id' => $creator->id, 'device_id' => $request->get('device_id')],
			    	['device_code' => $request->get('device_code')]
			    );

				$creator->load(['channels' => function ($query) {
					$query->with(['posts' => function ($query) {
						$query->with('postAttachment'); },
						'benefit' => function ($query) { 
						$query->with('planBenefit'); }
					]);
				}]);	
				
				$creator->profile_stats = app(CreatorController::class)->creatorProfileStats($creator->id, false);

    			return response()->json([
    				'response_code' => "1000",
    				'message' => 'Sign in successful',
    				'extra_payload' => $creator
    			]);	

    		}else{

    			return response()->json([
    				'response_code' => "1001",
    				'message' => 'Invalid credentials'
    			]);
    		}				
		}else {
    			return response()->json([
    				'response_code' => "1001",
    				'message' => 'Invalid credentials'
    			]);
		}

    }             



}
