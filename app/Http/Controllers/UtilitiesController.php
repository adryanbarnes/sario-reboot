<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Creator;
use App\Channel;
use App\UserInterest;
use App\Interest;
use App\User;
use App\Post;
use App\Comment;
use App\Like;
use App\Currency;
use App\PostStatus;
use App\PostType;
use App\PaymentMethod;
use App\PaymentStatus;


use Log;
use DB;
use Carbon\Carbon;
use Validator;

class UtilitiesController extends Controller
{
	public function getInterests()
	{
		$interests = Interest::with('category')->get();

		return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('interests' => $interests)]);
	}

    public function getCreators()
    {
    	$creators = Creator::select('id','name','profile_picture','description')->with('channels')->get();

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('creators' => $creators)]);
    }


    public function getConsumerRecommendations($consumerId)
    {
    	//recommended channels
    	//popular channels
    	//new channels
    	$new_channels = Channel::orderby('created_at','desc')->get()->take(5);

    	$user = User::find($consumerId);

    	$interest_ids = $user->UserInterests()->pluck('interest_id');
    	$cat_ids = Interest::whereIn('id',$interest_ids)->pluck('category_id');
    	$recommended_channels = Channel::with('creator')->whereIn('category_id',$cat_ids)->get();

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('new_channels' => $new_channels, 'recommended_channels' => $recommended_channels)]);
    }

    public function getCurrency(){

        $currencies = Currency::all();

        return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('currencies' => $currencies)]);
    }

    public function postComment(Request $request){

        $validator = Validator::make($request->all(),[
            'user_id'     => 'bail|nullable|exists:users,id',
            'creator_id'  => 'bail|required_without_all:user_id|exists:creators,id',
            'post_id'     => 'bail|required|exists:posts,id',
            'comment'   => 'bail|required|string',
        ]);    

        if ($validator->fails()) {

            return response()->json([
                'response_code' => '1001',
                'errors' => $validator->errors()
            ]);
        } 

        if($request->get('user_id')){
            $comment = Comment::create([
                'user_id' => $request->get('user_id'),
                'post_id'  => $request->get('post_id'),
                'message'  => $request->get('comment')
            ]);            
        }else{
            $comment = Comment::create([
                'creator_id' => $request->get('creator_id'),
                'post_id'  => $request->get('post_id'),
                'message'  => $request->get('comment')
            ]);            
        }

        $post = Post::with(['creator','postAttachment', 'channel'])->withCount('like', 'comment')
        ->find($request->get('post_id'));

        $liked =  Like::wherePostId($post->id);

        if ($request->get('creator_id')){
            $liked = $liked->whereCreatorId($request->get('creator_id'));
        }else{
            $liked = $liked->whereUserId($request->get('user_id'));
        }

        $liked = $liked->first();

		$post->liked = $liked ? true : false;       

        return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('comment' => $comment, 'post' => $post) ]);
    }

    
    public function unComment(Request $request){

        $validator = Validator::make($request->all(),[
            'user_id'     => 'bail|nullable|exists:users,id',
            'creator_id'  => 'bail|required_without_all:user_id|exists:creators,id',
            'comment_id'  => 'bail|required|exists:comments,id'
        ]);    

        if ($validator->fails()) {

            return response()->json([
                'response_code' => '1001',
                'errors' => $validator->errors()
            ]);
        } 

        if($request->get('user_id')){

            $comment = Comment::where('user_id', $request->get('user_id'))->find($request->get('comment_id'));
            
            $post = Post::find($comment->post_id);

            $post = Post::with(['creator','postAttachment', 'channel'])->withCount('like', 'comment')
            ->find($comment->post_id);
    
            $liked = Like::wherePostId($comment->post_id)->whereCreatorId($request->get('creator_id'))->first();
    
            $post->liked = $liked ? true : false;  

            $comment->delete();

        }else{
            $comment = Comment::where('creator_id', $request->get('creator_id'))->find($request->get('comment_id'));

            $post = Post::find($comment->post_id);

            $post = Post::with(['creator','postAttachment', 'channel'])->withCount('like', 'comment')
            ->find($comment->post_id);
    
            $liked = Like::wherePostId($comment->post_id)->whereUserId($request->get('user_id'))->first();
    
            $post->liked = $liked ? true : false;  

            $comment->delete();
        }

        return response()->json(['response_code' => '1000', 'response_message' => 'Comment successfully removed', 'extra_payload' => array( 'post' => $post) ]);
    }        

    public function postLike(Request $request){

        $validator = Validator::make($request->all(),[
            'user_id'  => 'bail|nullable|exists:users,id',
            'creator_id'  => 'bail|required_without_all:user_id|exists:creators,id',
            'post_id'  => 'bail|required|exists:posts,id'
        ]);    

        if ($validator->fails()) {

            return response()->json([
                'response_code' => '1001',
                'errors' => $validator->errors()
            ]);
        } 

        if($request->get('user_id')){
            $like = Like::create([
                'user_id' => $request->get('user_id'),
                'post_id'  => $request->get('post_id')
            ]);
        }else{
            $like = Like::create([
                'creator_id' => $request->get('creator_id'),
                'post_id'  => $request->get('post_id')
            ]);            
        }

        $post = Post::find($request->get('post_id'));

        $post = Post::with(['creator','postAttachment', 'channel'])->withCount('like', 'comment')
        ->find($request->get('post_id'));

        $liked =  Like::wherePostId($post->id);

        if ($request->get('creator_id')){
            $liked = $liked->whereCreatorId($request->get('creator_id'));
        }else{
            $liked = $liked->whereUserId($request->get('user_id'));
        }

        $liked = $liked->first();

		$post->liked = $liked ? true : false;          

        return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('like' => $like, 'post' => $post) ]);
    }

    public function unLike(Request $request){

        $validator = Validator::make($request->all(),[
            'user_id'  => 'bail|nullable|exists:users,id',
            'creator_id'  => 'bail|required_without_all:user_id|exists:creators,id',
            'post_id'  => 'bail|required|exists:posts,id'
        ]);    

        if ($validator->fails()) {

            return response()->json([
                'response_code' => '1001',
                'errors' => $validator->errors()
            ]);
        } 

        if($request->get('user_id')){
            Like::where('user_id', $request->get('user_id'))->where( 'post_id',$request->get('post_id'))->delete();
        }else{
            Like::where('creator_id', $request->get('creator_id'))->where('post_id',$request->get('post_id'))->delete();
        }

        $post = Post::with(['creator','postAttachment', 'channel'])->withCount('like', 'comment')
        ->find($request->get('post_id'));

        $liked =  Like::wherePostId($post->id);

        if ($request->get('creator_id')){
            $liked = $liked->whereCreatorId($request->get('creator_id'));
        }else{
            $liked = $liked->whereUserId($request->get('user_id'));
        }

        $liked = $liked->first();

		$post->liked = $liked ? true : false;  

        return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array( 'post' => $post) ]);
    } 

    public function getPostComments($postId, $userId, $type){

        $comments = Comment::where('post_id', $postId)->get();

        $comments->map(function ($item, $key) {

            if($item->creator_id != null){
                $item->comment_by = array(
                    'name' => $item->creator->name,
                    'id' => $item->creator->id,
                    'user_type' => 'creator',
                    'is_request_user' => true,
                    'profile_picture' => $item->creator->profile_picture

                );

            }else{
                $item->comment_by = array(
                    'name' => $item->user->name,
                    'id' => $item->user->id,
                    'user_type' => 'consumer',
                    'is_request_user' => false,
                    'profile_picture' => $item->user->profile_picture

                );
            }
            

            unset($item->creator_id);
            unset($item->user_id);
            unset($item->creator);
            unset($item->user);            
            return $item;
        });    
        
        $post = Post::with(['creator','postAttachment', 'channel'])->withCount('like', 'comment')
        ->find($postId);

        $liked =  Like::wherePostId($postId);

        if ($user = User::find($userId)){
            $liked = $liked->whereUserId($user->id);
        }else{
            $creator = Creator::find($userId);
            $liked = $liked->whereCreatorId($creator->id);
        }

        $liked = $liked->first();

		$post->liked = $liked ? true : false;         

        return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('comments' => $comments, 'post' => $post) ]);
    } 
    
    public function getPostLikes($postId, $userId, $type){

        $likes = Like::where('post_id', $postId)->get();

        $likes->map(function ($item, $key) {

            if($item->creator_id != null){
                $item->liked_by = array(
                    'name' => $item->creator->name,
                    'id' => $item->creator->id,
                    'user_type' => 'creator',
                    'is_request_user' =>  true,
                    'profile_picture' => $item->creator->profile_picture

                );

            }else{
                $item->liked_by = array(
                    'name' => $item->user->name,
                    'id' => $item->user->id,
                    'user_type' => 'consumer',
                    'is_request_user' => false,
                    'profile_picture' => $item->user->profile_picture

                );
            }
            

            unset($item->creator_id);
            unset($item->user_id);
            unset($item->creator);
            unset($item->user);            
            return $item;
        });

        $post = Post::with(['creator','postAttachment', 'channel'])->withCount('like', 'comment')
        ->find($postId);

        $liked =  Like::wherePostId($postId);

        if ($user = User::find($userId)){
            $liked = $liked->whereUserId($user->id);
        }else{
            $creator = Creator::find($userId);
            $liked = $liked->whereCreatorId($creator->id);
        }

        $liked = $liked->first();

		$post->liked = $liked ? true : false; 

        return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('likes' => $likes, 'post' => $post) ]);
    }     
    
    public function getPostStatus(){

        $postStatuses = PostStatus::all();

        return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('post_statuses' => $postStatuses)]);
    }   

	public function getPostTypes(){
    	$postTypes = PostType::all();

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('post_types' => $postTypes)]);     	
 		
    }
    
    public function paymentProviders(){
    	$paymentMethod = PaymentMethod::with('networks')->get();

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('payment_providers' => $paymentMethod)]);     	
 		        
    }

    public function paymentStatus(){
    	$paymentStatus = PaymentStatus::all();

    	return response()->json(['response_code' => '1000', 'response_message' => 'Success', 'extra_payload' => array('payment_status' => $paymentStatus)]);     	
 		        
    }    
    
}
