<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $guarded = [];

    protected $with = ['benefit'];

    protected $hidden = [
        'featured_channel_id'
    ];    
    
    public function creator(){
    	return $this->belongsTo('App\Creator');
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function benefit(){
        return $this->hasMany('App\SubscriptionPlan');
    }    

    public function category(){
        return $this->belongsTo('App\Category');
    }

}
