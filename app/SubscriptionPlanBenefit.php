<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPlanBenefit extends Model
{
    protected $guarded = [];

    public function subscriptionPlan(){
        return $this->belongsTo('App\SubscriptionPlan');
    }      
}
