<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    public function networks(){
        return $this->hasMany('App\Network');
    } 
}
