<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
    protected $guarded = [];

    protected $with = ['planBenefit'];

    public function planBenefit(){
        return $this->hasMany('App\SubscriptionPlanBenefit');
    }

    public function channel(){
        return $this->hasMany('App\Channel');
    }        

}
