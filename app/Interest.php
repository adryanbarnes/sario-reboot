<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $guarded = [];

    public function userInterest(){
		return $this->hasMany('App\UserInterest');    	
    }

    public function category(){
		return $this->belongsTo('App\Category');    	
    }
}
