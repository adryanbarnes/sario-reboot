<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function genre(){
    	return $this->hasMany('App\Interest');
    }
}
