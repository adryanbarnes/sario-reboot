<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    public function paymentTransaction(){
    	return $this->belongsToMany('App\PaymentTransaction');
    } 
}
