<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => false,
      'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAA6kvk-MA:APA91bGzci1MRQWYeY2pXk5G1m6e9MyU0_rlO_K_q1_TwXAJlWCkCj2ev679qxjiSxj9B7xjYqdaDSHKXSrRE1-hxR8eI-NAaO9EGci3wr9y-eoESCaiVGsty16jMYxLWaPB4x1WDRtY',
        //'apiKey' => 'AAAA0zgprVM:APA91bGv80mbgJBNenLTe4DH4JrMtN-GqcvLYlQSuuLC4J8vGoYX5SHjgc7gt3a2DdBE31-lLE1SegDXunGhcjOAmMf1U1zEUadJ_CILi8IIwgpqg8DyUetCF6_P0t6kkJNeHkC1xMxe',
  ],
  'apn' => [
      'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
      'passPhrase' => '1234', //Optional
      'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
      'dry_run' => true
  ]
];